#import <Foundation/Foundation.h>
#import <UIKit/UIDevice.h>
#define IFPGA_NAMESTRING    @"iFPGA"
 
#define IPHONE_1G_NAMESTRING   @"iPhone 1G"
#define IPHONE_3G_NAMESTRING   @"iPhone 3G"
#define IPHONE_3GS_NAMESTRING   @"iPhone 3GS" 
#define IPHONE_4_NAMESTRING    @"iPhone 4" 
#define IPHONE_4S_NAMESTRING   @"iPhone 4S" 
#define IPHONE_5_NAMESTRING    @"iPhone 5"
#define IPHONE_UNKNOWN_NAMESTRING  @"Unknown iPhone"
 
#define IPOD_1G_NAMESTRING    @"iPod touch 1G"
#define IPOD_2G_NAMESTRING    @"iPod touch 2G"
#define IPOD_3G_NAMESTRING    @"iPod touch 3G"
#define IPOD_4G_NAMESTRING    @"iPod touch 4G"
#define IPOD_UNKNOWN_NAMESTRING   @"Unknown iPod"
 
#define IPAD_1G_NAMESTRING    @"iPad 1G"
#define IPAD_2G_NAMESTRING    @"iPad 2G"
#define IPAD_3G_NAMESTRING    @"iPad 3G"
#define IPAD_UNKNOWN_NAMESTRING   @"Unknown iPad"
 
// Nano? Apple TV?
#define APPLETV_2G_NAMESTRING   @"Apple TV 2G"
 
#define IPOD_FAMILY_UNKNOWN_DEVICE   @"Unknown iOS device"
 
#define IPHONE_SIMULATOR_NAMESTRING   @"iPhone Simulator"
#define IPHONE_SIMULATOR_IPHONE_NAMESTRING @"iPhone Simulator"
#define IPHONE_SIMULATOR_IPAD_NAMESTRING @"iPad Simulator"
 
typedef enum {
 UIDeviceUnknown,
  
 UIDeviceiPhoneSimulator,
 UIDeviceiPhoneSimulatoriPhone, // both regular and iPhone 4 devices
 UIDeviceiPhoneSimulatoriPad,
  
 UIDevice1GiPhone,
 UIDevice3GiPhone,
 UIDevice3GSiPhone,
 UIDevice4iPhone,
    UIDevice4SiPhone,
 UIDevice5iPhone,
  
 UIDevice1GiPod,
 UIDevice2GiPod,
 UIDevice3GiPod,
 UIDevice4GiPod,
  
 UIDevice1GiPad, // both regular and 3G
 UIDevice2GiPad,
    UIDevice3GiPad,
  
  
 UIDeviceAppleTV2,
  
 UIDeviceUnknowniPhone,
 UIDeviceUnknowniPod,
 UIDeviceUnknowniPad,
 UIDeviceIFPGA,
     
} UIDevicePlatform;
 
 
 
 
@interface UIDevice (IdentifierAddition)
 
 
 
/*
 * @method uniqueDeviceIdentifier
 * @description use this method when you need a unique identifier in one app.
 * It generates a hash from the MAC-address in combination with the bundle identifier
 * of your app.
 */
 
- (NSString *) uniqueDeviceIdentifier;
 
/*
 * @method uniqueGlobalDeviceIdentifier
 * @description use this method when you need a unique global identifier to track a device
 * with multiple apps. as example a advertising network will use this method to track the device
 * from different apps.
 * It generates a hash from the MAC-address only.
 */
 
- (NSString *) uniqueGlobalDeviceIdentifier;
 
 
- (NSString *) platformString;
 
 
@end
 
 
 
 
#import "UIDevice+IdentifierAddition.h"
#import "NSString+MD5Addition.h"
#import <UIKit/UIScreen.h>
 
#import <sys/socket.h> // Per msqr
#import <sys/sysctl.h>
#import <net/if.h>
#import <net/if_dl.h>
 
@interface UIDevice(Private)
 
- (NSString *) getMacAddress;
 
@end
 
@implementation UIDevice (IdentifierAddition)
 
////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Private Methods
 
// Return the local MAC addy
// Courtesy of FreeBSD hackers email list
// Accidentally munged during previous update. Fixed thanks to erica sadun & mlamb.
- (NSString *) getMacAddress{
     
    int                 mib[6];
    size_t              len;
    char                *buf;
    unsigned char       *ptr;
    struct if_msghdr    *ifm;
    struct sockaddr_dl  *sdl;
     
    mib[0] = CTL_NET;
    mib[1] = AF_ROUTE;
    mib[2] = 0;
    mib[3] = AF_LINK;
    mib[4] = NET_RT_IFLIST;
     
    if ((mib[5] = if_nametoindex("en0")) == 0) {
        printf("Error: if_nametoindex error\n");
        return NULL;
    }
     
    if (sysctl(mib, 6, NULL, &len;, NULL, 0) < 0) {
        printf("Error: sysctl, take 1\n");
        return NULL;
    }
     
    if ((buf = malloc(len)) == NULL) {
        printf("Could not allocate memory. error!\n");
        return NULL;
    }
     
    if (sysctl(mib, 6, buf, &len;, NULL, 0) < 0) {
        printf("Error: sysctl, take 2");
        free(buf);
        return NULL;
    }
     
    ifm = (struct if_msghdr *)buf;
    sdl = (struct sockaddr_dl *)(ifm + 1);
    ptr = (unsigned char *)LLADDR(sdl);
    NSString *outstring = [NSString stringWithFormat:@"X:X:X:X:X:X", 
                           *ptr, *(ptr+1), *(ptr+2), *(ptr+3), *(ptr+4), *(ptr+5)];
    free(buf);
     
    return outstring;
}
 
////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Public Methods
 
- (NSString *) uniqueDeviceIdentifier{
    NSString *macaddress = [[UIDevice currentDevice] getMacAddress];
    NSString *bundleIdentifier = [[NSBundle mainBundle] bundleIdentifier];
     
    NSString *stringToHash = [NSString stringWithFormat:@"%@%@",macaddress,bundleIdentifier];
    NSString *uniqueIdentifier = [stringToHash stringFromMD5];
     
    return uniqueIdentifier;
}
 
- (NSString *) uniqueGlobalDeviceIdentifier{
    NSString *macaddress = [[UIDevice currentDevice] getMacAddress];
    NSString *uniqueIdentifier = [macaddress stringFromMD5];
     
    return uniqueIdentifier;
}
 
#pragma mark sysctlbyname utils
- (NSString *) getSysInfoByName:(char *)typeSpecifier
{
 size_t size;
    sysctlbyname(typeSpecifier, NULL, &size;, NULL, 0);
    char *answer = malloc(size);
 sysctlbyname(typeSpecifier, answer, &size;, NULL, 0);
 NSString *results = [NSString stringWithCString:answer encoding: NSUTF8StringEncoding];
 free(answer);
 return results;
}
 
- (NSString *) platform
{
 return [self getSysInfoByName:"hw.machine"];
}
 
#pragma mark platform type and name utils
- (NSUInteger) platformType
{
 NSString *platform = [self platform];
 // if ([platform isEqualToString:@"XX"])   return UIDeviceUnknown;
 if ([platform isEqualToString:@"iFPGA"])  return UIDeviceIFPGA;
     
 if ([platform isEqualToString:@"iPhone1,1"]) return UIDevice1GiPhone;
 if ([platform isEqualToString:@"iPhone1,2"]) return UIDevice3GiPhone;
 if ([platform hasPrefix:@"iPhone2"])         return UIDevice3GSiPhone;
 if ([platform isEqualToString:@"iPhone3,1"])   return UIDevice4iPhone;
    if ([platform isEqualToString:@"iPhone4,1"])   return UIDevice4SiPhone;
 
 if ([platform hasPrefix:@"iPhone5"])   return UIDevice5iPhone;
    
  
 if ([platform isEqualToString:@"iPod1,1"])   return UIDevice1GiPod;
 if ([platform isEqualToString:@"iPod2,1"])   return UIDevice2GiPod;
 if ([platform isEqualToString:@"iPod3,1"])   return UIDevice3GiPod;
 if ([platform isEqualToString:@"iPod4,1"])   return UIDevice4GiPod;
     
 if ([platform isEqualToString:@"iPad1,1"])   return UIDevice1GiPad;
 if ([platform isEqualToString:@"iPad2,1"])   return UIDevice2GiPad;
 if ([platform isEqualToString:@"iPad3,1"])   return UIDevice3GiPad;
     
    if ([platform isEqualToString:@"iPad2,4"])   return UIDevice2GiPad;
  
 if ([platform isEqualToString:@"AppleTV2,1"]) return UIDeviceAppleTV2;
  
 /*
  MISSING A SOLUTION HERE TO DATE TO DIFFERENTIATE iPAD and iPAD 3G.... SORRY!
  */
     
 if ([platform hasPrefix:@"iPhone"]) return UIDeviceUnknowniPhone;
 if ([platform hasPrefix:@"iPod"]) return UIDeviceUnknowniPod;
 if ([platform hasPrefix:@"iPad"]) return UIDeviceUnknowniPad;
  
 if ([platform hasSuffix:@"86"] || [platform isEqual:@"x86_64"])
 {
  if ([[UIScreen mainScreen] bounds].size.width < 768)
   return UIDeviceiPhoneSimulatoriPhone;
  else
   return UIDeviceiPhoneSimulatoriPad;
         
  return UIDeviceiPhoneSimulator;
 }
 return UIDeviceUnknown;
}
 
- (NSString *) platformString
{
 switch ([self platformType])
 {
  case UIDevice1GiPhone: return IPHONE_1G_NAMESTRING;
  case UIDevice3GiPhone: return IPHONE_3G_NAMESTRING;
  case UIDevice3GSiPhone: return IPHONE_3GS_NAMESTRING;
  case UIDevice4iPhone: return IPHONE_4_NAMESTRING;
        case UIDevice4SiPhone: return IPHONE_4S_NAMESTRING;
  case UIDevice5iPhone: return IPHONE_5_NAMESTRING;
  case UIDeviceUnknowniPhone: return IPHONE_UNKNOWN_NAMESTRING;
             
  case UIDevice1GiPod: return IPOD_1G_NAMESTRING;
  case UIDevice2GiPod: return IPOD_2G_NAMESTRING;
  case UIDevice3GiPod: return IPOD_3G_NAMESTRING;
  case UIDevice4GiPod: return IPOD_4G_NAMESTRING;
  case UIDeviceUnknowniPod: return IPOD_UNKNOWN_NAMESTRING;
    
  case UIDevice1GiPad : return IPAD_1G_NAMESTRING;
  case UIDevice2GiPad : return IPAD_2G_NAMESTRING;
        case UIDevice3GiPad : return IPAD_3G_NAMESTRING;
             
    
  case UIDeviceAppleTV2 : return APPLETV_2G_NAMESTRING;
    
  case UIDeviceiPhoneSimulator: return IPHONE_SIMULATOR_NAMESTRING;
  case UIDeviceiPhoneSimulatoriPhone: return IPHONE_SIMULATOR_IPHONE_NAMESTRING;
  case UIDeviceiPhoneSimulatoriPad: return IPHONE_SIMULATOR_IPAD_NAMESTRING;
    
  case UIDeviceIFPGA: return IFPGA_NAMESTRING;
    
  default: return IPOD_FAMILY_UNKNOWN_DEVICE;
 }
}
 
 
@end